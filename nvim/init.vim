" dependencies {{{
" - node python3
" - git
" - devicons
" - devicons compatible font (i.e. patched nerd font)
" - some form of ctags
" - ripgrep (ideally)
" }}}

" definitions {{{
" if &compatible
    set nocompatible " be iMproved
"  endif

let s:is_windows = has('win32') || has('win64') || has('win16') || has('dos32') || has('dos16')
let s:is_nvim = has('nvim')
let s:is_oni = exists('g:gui_oni')

" vim sourcing compatibility
"if (s:is_windows && s:is_nvim)
"    set rtp^=~/.vim rtp+=/.vim/after
"    let &packpath = &runtimepath
"    source ~/.vimrc
"endif
" }}}

" vim-plug set up {{{
let s:vim_plug_dir = s:is_windows ? glob('~/AppData/Local/nvim/autoload/plug.vim') : glob('~/.vim/autoload/plug.vim')
if !filereadable(expand(s:vim_plug_dir))
  silent execute "!curl -fLo " + s:vim_plug_dir  + " --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif
" }}}

" plugins {{{
call plug#begin('~/.vim/plugged')

" general {{{
Plug 'equalsraf/neovim-gui-shim'
Plug 'sheerun/vim-polyglot'
Plug 'urbainvaes/vim-remembrall' " recall bindings
" Plug 'freitass/todo.txt-vim'
" Plug 'mhinz/vim-sayonara' " for easier buffer management
" }}}

" themes {{{
Plug 'herrbischoff/cobalt2.vim'
Plug 'NLKNguyen/papercolor-theme'
Plug 'joshdick/onedark.vim' " a nvim specific theme, but works on vim too
Plug 'tomasr/molokai'
Plug 'mhartington/oceanic-next'
Plug 'icymind/NeoSolarized'
" }}}

" ui {{{
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

Plug 'itchyny/lightline.vim'

Plug 'machakann/vim-highlightedyank'
Plug 'markonm/traces.vim'

Plug 'luochen1990/rainbow'
" let g:rainbow_active = 1 " is this needed?
Plug 'Yggdroot/indentLine'

Plug 'junegunn/goyo.vim'
" Plug 'junegunn/limelight.vim'
" }}}

" tags {{{
" Plug 'ludovicchabant/vim-gutentags'
" let g:gutentags_cache_dir=s:is_windows ? "C:/temp/" : "/tmp"
"npm install --global git+https://github.com/Perlence/tstags.git
" Plug 'majutsushi/tagbar'
" }}}

" easy editing :D {{{ 
" Plug 'scrooloose/nerdcommenter'
" let g:NERDTrimTrailingWhitespace = 1
" TODO: try the other two commenters, more minimal
Plug 'tomtom/tcomment_vim'
" Plug 'tpope/vim-commentary'

Plug 'editorconfig/editorconfig-vim'
let g:EditorConfig_exclude_patterns = ['fugitive://.*']

"Plug 'alvan/vim-closetag'

"Plug 'junegunn/vim-easy-align'

Plug 'junegunn/vim-peekaboo'
"Plug 'mbbill/undotree'

"Plug 'sbdchd/neoformat'
Plug 'tpope/vim-sleuth' " tries to detect file settings, maybe non plugin: https://www.vim.org/scripts/script.php?script_id=1171

" Plug 'tpope/vim-unimpaired'

" Plug 'tpope/vim-endwise'
" Plug 'tpope/vim-surround' " change brackets, tags, quotes... done with commands
" " Plug 'jiangmiao/auto-pairs' " also brackets, but matching them as you type,
" has issues with comments here.. because quotes, investigate more. Toggle off
" with <M-p>

" Plug 'terryma/vim-multiple-cursors' " should try to use macros instead
" }}}

" navigation {{{
" Plug 'justinmk/vim-dirvish'
" Plug 'junegunn/vim-slash' " removes highlight after, also centers result
" (like my binding below)
" Plug 'haya14busa/is.vim' " similar to vim slash

" Vim-Session
Plug 'xolox/vim-misc' " vim-session dependends on this
Plug 'xolox/vim-session'
let g:session_autosave='no'

Plug 'eugen0329/vim-esearch'
" Plug 'Konfekt/FastFold'
" Plug 'zhimsel/vim-stay'

" Plug 'ipod825/vim-netranger' " kinda broken, needs to disable nerdtree too

" Plug 'ajh17/VimCompletesMe' " like supertab?
" Plug 'ervandew/supertab' " hangs if not before some other plugins, also binding clash
" let g:SuperTabDefaultCompletionType = "<C-n>"
" let g:SuperTabContextDefaultCompletionType = "<c-n>"
" " close the preview window when you're not using it
" let g:SuperTabClosePreviewOnPopupClose = 1
" https://www.gregjs.com/vim/2016/neovim-deoplete-jspc-ultisnips-and-tern-a-config-for-kickass-autocompletion/

" }}}

" git stuff {{{
" https://www.reddit.com/r/vim/comments/7z68bl/using_vim_to_view_git_commits/
" Plug 'tpope/vim-fugitive'
Plug 'neoclide/vim-easygit' " better than fugitive, replacable?, and can be
" used with denite? (via another plugin)?
let g:easygit_enable_command = 1
" Plug 'lambdalisue/gina.vim' " beta, don't know what it does
" Plug 'jreybert/vimagit' " different paradigm, might be ok
Plug 'junegunn/gv.vim' " requires fugitive, branch viewer
Plug 'rhysd/committia.vim' " when git's default editor is neovim...
" Plug 'mhinz/vim-signify' " vs gitgutter?, but supports more VCS
Plug 'airblade/vim-gitgutter'
Plug 'Xuyuanp/nerdtree-git-plugin'
" }}}

" fuzzy finding {{{
Plug 'Shougo/denite.nvim'
Plug 'Shougo/neomru.vim'
Plug 'neoclide/denite-git' " requires neoclide/vim-easygit
Plug 'neoclide/denite-extra'
" Plug 'junegunn/fzf.vim'
" }}}

" lint {{{
Plug 'w0rp/ale'
" }}}

" completion + snippets {{{
" Plug 'lifepillar/vim-mucomplete' " pretty legit

" This one integrates with deoplete (or NCM2, etc...)
"Plug 'prabirshrestha/vim-lsp'

" Plug 'autozimu/LanguageClient-neovim', {
"     \ 'branch': 'next',
"     \ 'do': s:is_windows ? 'powershell -executionpolicy bypass -File install.ps1' : 'sh ./install.sh',
"     \ }

" deoplete can use LSP style too 
Plug 'Shougo/deoplete.nvim', { 'do' : ':UpdateRemotePlugins' }
Plug 'Shougo/echodoc.vim'
" Plug 'rudism/deoplete-tsuquyomi'
" Plug 'Shougo/deoppet' " when available, replace neosnippet
Plug 'Shougo/neosnippet'
Plug 'Shougo/neosnippet-snippets'
Plug 'Shougo/neoinclude.vim'

" Plug 'neoclide/coc.nvim' " general completion? has denite source
" Plug 'honza/vim-snippets'
" }}}

" lang {{{
" html & css {{{
Plug 'mattn/emmet-vim'
" Plug 'chrisbra/Colorizer'
Plug 'gko/vim-coloresque'
" }}}

" csharp {{{
Plug 'omnisharp/omnisharp-vim', { 'for': 'cs' } " requires python, vim-dispatch OR vimproc
" Plug 'cyansprite/deoplete-omnisharp' " not really working, but it starts the omnisharp server it comes with
" Plug 'Robzz/deoplete-omnisharp/', { 'for': 'cs', 'do': ':UpdateRemotePlugins' }
" }}}

" typescript {{{
" Plug 'HerringtonDarkholme/yats.vim' " seems a bit worse, doesn't highlight `this`
Plug 'leafgarland/typescript-vim' " this one is already in vim polyglot
" Below also hangs (maybe deoplete? omnifunc?):/ now has ./install.sh (goes to rplugin and does npm i && npm run build)
Plug 'mhartington/nvim-typescript', { 'do' : ':UpdateRemotePlugins' }

" Plug 'quramy/tsuquyomi', { 'for' : 'typescript' }
" Plug 'shougo/vimproc.vim', {'do' : 'make'} " tsuquyomi needs (also omnisharp)

Plug 'peitalin/vim-jsx-typescript'
" Plug 'runoshun/tscompletejob'
" }}}
" }}}


" Must be last... TODO set GuiFont in ginit.vim
" "FuraCode NF", "mononoki NF"
" Plug 'ryanoasis/vim-devicons'
" if (!s:is_nvim)
  " set encoding=UTF-8
" endif
call plug#end()
" }}}

" plugin settings {{{ 

let g:OmniSharp_server_stdio = 1
let g:deoplete#enable_at_startup=1
"call denite#custom#var('file_rec', 'command',
"            \ ['pt', '--follow', '--nocolor', '--nogroup',
"            \  (s:is_windows ? '-g:' : '-g='), ''])
" Define mappings
autocmd FileType denite call s:denite_my_settings()
function! s:denite_my_settings() abort
  nnoremap <silent><buffer><expr> <CR>
  \ denite#do_map('do_action')
  nnoremap <silent><buffer><expr> d
  \ denite#do_map('do_action', 'delete')
  nnoremap <silent><buffer><expr> p
  \ denite#do_map('do_action', 'preview')
  nnoremap <silent><buffer><expr> q
  \ denite#do_map('quit')
  nnoremap <silent><buffer><expr> i
  \ denite#do_map('open_filter_buffer')
  nnoremap <silent><buffer><expr> <Space>
  \ denite#do_map('toggle_select').'j'
endfunction
			
" let g:LanguageClient_serverCommands = {
"     " \ 'fsharp': ['dotnet', 'D:/opt/fsharp-language-server/src/FSharpLanguageServer/bin/Release/target/FSharpLanguageServer.dll']
"     \ }

let g:NERDSpaceDelims = 1
let g:NERDTreeShowBookmarks=1
let g:NERDTreeShowHidden=1

let g:goyo_width=110
let g:goyo_height=90

" let g:tagbar_iconchars = ['↠', '↡']
let g:tagbar_type_typescript = {                                                  
  \ 'ctagsbin' : 'tstags',                                                        
  \ 'ctagsargs' : '-f-',                                                           
  \ 'kinds': [                                                                     
	\ 'e:enums:0:1',                                                               
	\ 'f:function:0:1',                                                            
	\ 't:typealias:0:1',                                                           
	\ 'M:Module:0:1',                                                              
	\ 'I:import:0:1',                                                              
	\ 'i:interface:0:1',                                                           
	\ 'C:class:0:1',                                                               
	\ 'm:method:0:1',
	\ 'p:property:0:1',                                                            
	\ 'v:variable:0:1',                                                            
	\ 'c:const:0:1',                                                              
  \ ],                                                                            
  \ 'sort' : 0                                                                    
\ }  
" }}}

" system settings {{{
filetype plugin indent on
syntax on
set title
set ignorecase smartcase
set nu relativenumber
set clipboard^=unnamed,unnamedplus
set expandtab " tabstop=4 softtabstop=4 shiftwidth=4
set backspace=indent,eol,start
set ruler
set cursorline

set cmdheight=2 " for echodoc
set foldlevelstart=0

" python
" chomp the newline char
function! SystemChomp(...)
  return substitute(call('system', a:000), '\n\+$', '', '')
endfunction
" use which (or where on windows)
" TODO: maybe move to env inside neovim
let g:python3_host_prog=s:is_windows ? 'C:/Python37/python.exe' : SystemChomp("which python3")
let g:python_host_prog=s:is_windows ? 'C:/Python27/python.exe' : SystemChomp("which python2")

" }}}

" plugin key map {{{
" nnoremap <F8> :TagbarToggle<CR> " TODO: deprecate
" nnoremap <silent> <localleader>t :TagbarToggle<CR>

nnoremap <silent> <C-p> :DeniteProjectDir file/rec<CR>
nnoremap <silent> <F2> :ALENextWrap<CR> 
nnoremap <silent> <C-.> :ALEFix<CR>

nnoremap <silent> <C-e> :NERDTreeToggle<CR>

" TODO langage client mappings
" }}}

" system key map {{{
inoremap fj <esc>
inoremap jf <esc>

" easy tab switching
nnoremap <A-h> :tabp<CR>
nnoremap <A-l> :tabn<CR>
" nnoremap <silent> <S-t> :tabnew<CR>

" easy window switching
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" go down a visible line (wrapped)
noremap <silent> <buffer> k gk
noremap <silent> <buffer> j gj

" search mappings: these will make it so that going to the next one in a
" search will center on the line it's found in.
" vim-slash does this too
nnoremap n nzzzv
nnoremap N Nzzzv
" }}}

" theme {{{
colorscheme OceanicNext
" colorscheme cobalt2
let g:lightline = { 'colorscheme': 'onedark' }
" }}}

