" <TAB>: completion.
" inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
" inoremap <expr><S-TAB>  pumvisible() ? "\<C-p>" : "\<S-TAB>"

" easy escape
inoremap fj <esc>
inoremap jf <esc>
vnoremap fj <esc>
vnoremap jf <esc>

" easy tab switching
nnoremap <A-h> :tabp<CR>
nnoremap <A-l> :tabn<CR>

" easy window switching
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" easy window resizing
" nnoremap <C-,> <C-w><
" nnoremap <C-.> <C-w>>

" go down a visible line (wrapped)
noremap <silent> <buffer> k gk
noremap <silent> <buffer> j gj

nnoremap <silent> <C-p> :DeniteProjectDir file_rec<CR>
